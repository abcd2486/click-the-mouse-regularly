namespace MouseClicker
{
    partial class CheckForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckForm));
            this.button_Start = new System.Windows.Forms.Button();
            this.checkBox_1 = new System.Windows.Forms.CheckBox();
            this.checkBox_2 = new System.Windows.Forms.CheckBox();
            this.checkBox_3 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_x1 = new System.Windows.Forms.TextBox();
            this.textBox_x2 = new System.Windows.Forms.TextBox();
            this.textBox_x3 = new System.Windows.Forms.TextBox();
            this.textBox_y1 = new System.Windows.Forms.TextBox();
            this.textBox_y2 = new System.Windows.Forms.TextBox();
            this.textBox_y3 = new System.Windows.Forms.TextBox();
            this.textBox_y0 = new System.Windows.Forms.TextBox();
            this.textBox_x0 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button_pick = new System.Windows.Forms.Button();
            this.dateTimePicker_1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_3 = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label_Time = new System.Windows.Forms.Label();
            this.CheckBox_Type = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_Times1 = new System.Windows.Forms.TextBox();
            this.textBox_Interval1 = new System.Windows.Forms.TextBox();
            this.textBox_Times2 = new System.Windows.Forms.TextBox();
            this.textBox_Interval2 = new System.Windows.Forms.TextBox();
            this.textBox_Times3 = new System.Windows.Forms.TextBox();
            this.textBox_Interval3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox_Interval6 = new System.Windows.Forms.TextBox();
            this.textBox_Times6 = new System.Windows.Forms.TextBox();
            this.textBox_Interval5 = new System.Windows.Forms.TextBox();
            this.textBox_Times5 = new System.Windows.Forms.TextBox();
            this.textBox_Interval4 = new System.Windows.Forms.TextBox();
            this.textBox_Times4 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.dateTimePicker_6 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_5 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_4 = new System.Windows.Forms.DateTimePicker();
            this.textBox_y6 = new System.Windows.Forms.TextBox();
            this.textBox_y5 = new System.Windows.Forms.TextBox();
            this.textBox_y4 = new System.Windows.Forms.TextBox();
            this.textBox_x6 = new System.Windows.Forms.TextBox();
            this.textBox_x5 = new System.Windows.Forms.TextBox();
            this.textBox_x4 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.checkBox_6 = new System.Windows.Forms.CheckBox();
            this.checkBox_5 = new System.Windows.Forms.CheckBox();
            this.checkBox_4 = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox_Handle = new System.Windows.Forms.TextBox();
            this.button_GETHandle = new System.Windows.Forms.Button();
            this.checkBox_Handle = new System.Windows.Forms.CheckBox();
            this.textBox_HandleName = new System.Windows.Forms.TextBox();
            this.textBox_Handle1 = new System.Windows.Forms.TextBox();
            this.textBox_Handle2 = new System.Windows.Forms.TextBox();
            this.textBox_Handle3 = new System.Windows.Forms.TextBox();
            this.textBox_Handle4 = new System.Windows.Forms.TextBox();
            this.textBox_Handle5 = new System.Windows.Forms.TextBox();
            this.textBox_Handle6 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(434, 289);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(75, 23);
            this.button_Start.TabIndex = 0;
            this.button_Start.Text = "开始";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // checkBox_1
            // 
            this.checkBox_1.AutoSize = true;
            this.checkBox_1.Checked = true;
            this.checkBox_1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_1.Location = new System.Drawing.Point(9, 125);
            this.checkBox_1.Name = "checkBox_1";
            this.checkBox_1.Size = new System.Drawing.Size(54, 16);
            this.checkBox_1.TabIndex = 2;
            this.checkBox_1.Text = "定时1";
            this.checkBox_1.UseVisualStyleBackColor = true;
            // 
            // checkBox_2
            // 
            this.checkBox_2.AutoSize = true;
            this.checkBox_2.Location = new System.Drawing.Point(9, 149);
            this.checkBox_2.Name = "checkBox_2";
            this.checkBox_2.Size = new System.Drawing.Size(54, 16);
            this.checkBox_2.TabIndex = 3;
            this.checkBox_2.Text = "定时2";
            this.checkBox_2.UseVisualStyleBackColor = true;
            // 
            // checkBox_3
            // 
            this.checkBox_3.AutoSize = true;
            this.checkBox_3.Location = new System.Drawing.Point(9, 174);
            this.checkBox_3.Name = "checkBox_3";
            this.checkBox_3.Size = new System.Drawing.Size(54, 16);
            this.checkBox_3.TabIndex = 4;
            this.checkBox_3.Text = "定时3";
            this.checkBox_3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "坐标 x:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "坐标 x: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "坐标 x:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "y:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(163, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "y:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(163, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "y:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(235, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "定时:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(235, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "定时:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(235, 176);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "定时:";
            // 
            // textBox_x1
            // 
            this.textBox_x1.Location = new System.Drawing.Point(112, 123);
            this.textBox_x1.Name = "textBox_x1";
            this.textBox_x1.Size = new System.Drawing.Size(46, 21);
            this.textBox_x1.TabIndex = 14;
            this.textBox_x1.Text = "0";
            // 
            // textBox_x2
            // 
            this.textBox_x2.Location = new System.Drawing.Point(112, 147);
            this.textBox_x2.Name = "textBox_x2";
            this.textBox_x2.Size = new System.Drawing.Size(46, 21);
            this.textBox_x2.TabIndex = 15;
            this.textBox_x2.Text = "0";
            // 
            // textBox_x3
            // 
            this.textBox_x3.Location = new System.Drawing.Point(112, 171);
            this.textBox_x3.Name = "textBox_x3";
            this.textBox_x3.Size = new System.Drawing.Size(46, 21);
            this.textBox_x3.TabIndex = 16;
            this.textBox_x3.Text = "0";
            // 
            // textBox_y1
            // 
            this.textBox_y1.Location = new System.Drawing.Point(181, 123);
            this.textBox_y1.Name = "textBox_y1";
            this.textBox_y1.Size = new System.Drawing.Size(46, 21);
            this.textBox_y1.TabIndex = 17;
            this.textBox_y1.Text = "0";
            // 
            // textBox_y2
            // 
            this.textBox_y2.Location = new System.Drawing.Point(181, 147);
            this.textBox_y2.Name = "textBox_y2";
            this.textBox_y2.Size = new System.Drawing.Size(46, 21);
            this.textBox_y2.TabIndex = 18;
            this.textBox_y2.Text = "0";
            // 
            // textBox_y3
            // 
            this.textBox_y3.Location = new System.Drawing.Point(181, 171);
            this.textBox_y3.Name = "textBox_y3";
            this.textBox_y3.Size = new System.Drawing.Size(46, 21);
            this.textBox_y3.TabIndex = 19;
            this.textBox_y3.Text = "0";
            // 
            // textBox_y0
            // 
            this.textBox_y0.Location = new System.Drawing.Point(150, 266);
            this.textBox_y0.Name = "textBox_y0";
            this.textBox_y0.Size = new System.Drawing.Size(46, 21);
            this.textBox_y0.TabIndex = 23;
            // 
            // textBox_x0
            // 
            this.textBox_x0.Location = new System.Drawing.Point(81, 266);
            this.textBox_x0.Name = "textBox_x0";
            this.textBox_x0.Size = new System.Drawing.Size(46, 21);
            this.textBox_x0.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(131, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "y:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "获取坐标 x:";
            // 
            // button_pick
            // 
            this.button_pick.Location = new System.Drawing.Point(204, 265);
            this.button_pick.Name = "button_pick";
            this.button_pick.Size = new System.Drawing.Size(21, 23);
            this.button_pick.TabIndex = 24;
            this.button_pick.Text = "S";
            this.button_pick.UseVisualStyleBackColor = true;
            this.button_pick.Click += new System.EventHandler(this.button_pick_Click);
            // 
            // dateTimePicker_1
            // 
            this.dateTimePicker_1.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_1.Location = new System.Drawing.Point(276, 124);
            this.dateTimePicker_1.Name = "dateTimePicker_1";
            this.dateTimePicker_1.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_1.TabIndex = 25;
            this.dateTimePicker_1.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // dateTimePicker_2
            // 
            this.dateTimePicker_2.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_2.Location = new System.Drawing.Point(276, 148);
            this.dateTimePicker_2.Name = "dateTimePicker_2";
            this.dateTimePicker_2.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_2.TabIndex = 26;
            this.dateTimePicker_2.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // dateTimePicker_3
            // 
            this.dateTimePicker_3.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_3.Location = new System.Drawing.Point(276, 171);
            this.dateTimePicker_3.Name = "dateTimePicker_3";
            this.dateTimePicker_3.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_3.TabIndex = 27;
            this.dateTimePicker_3.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(501, 114);
            this.textBox1.TabIndex = 28;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Location = new System.Drawing.Point(8, 294);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(47, 12);
            this.label_Time.TabIndex = 29;
            this.label_Time.Text = "label12";
            // 
            // CheckBox_Type
            // 
            this.CheckBox_Type.AutoSize = true;
            this.CheckBox_Type.Location = new System.Drawing.Point(317, 295);
            this.CheckBox_Type.Name = "CheckBox_Type";
            this.CheckBox_Type.Size = new System.Drawing.Size(72, 16);
            this.CheckBox_Type.TabIndex = 30;
            this.CheckBox_Type.Text = "消息方式";
            this.CheckBox_Type.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(381, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 31;
            this.label12.Text = "次数";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(446, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "间隔";
            // 
            // textBox_Times1
            // 
            this.textBox_Times1.Location = new System.Drawing.Point(412, 126);
            this.textBox_Times1.Name = "textBox_Times1";
            this.textBox_Times1.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times1.TabIndex = 33;
            this.textBox_Times1.Text = "0";
            // 
            // textBox_Interval1
            // 
            this.textBox_Interval1.Location = new System.Drawing.Point(475, 126);
            this.textBox_Interval1.Name = "textBox_Interval1";
            this.textBox_Interval1.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval1.TabIndex = 34;
            this.textBox_Interval1.Text = "0";
            // 
            // textBox_Times2
            // 
            this.textBox_Times2.Location = new System.Drawing.Point(412, 150);
            this.textBox_Times2.Name = "textBox_Times2";
            this.textBox_Times2.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times2.TabIndex = 35;
            this.textBox_Times2.Text = "0";
            // 
            // textBox_Interval2
            // 
            this.textBox_Interval2.Location = new System.Drawing.Point(475, 150);
            this.textBox_Interval2.Name = "textBox_Interval2";
            this.textBox_Interval2.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval2.TabIndex = 36;
            this.textBox_Interval2.Text = "0";
            // 
            // textBox_Times3
            // 
            this.textBox_Times3.Location = new System.Drawing.Point(412, 174);
            this.textBox_Times3.Name = "textBox_Times3";
            this.textBox_Times3.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times3.TabIndex = 37;
            this.textBox_Times3.Text = "0";
            // 
            // textBox_Interval3
            // 
            this.textBox_Interval3.Location = new System.Drawing.Point(475, 174);
            this.textBox_Interval3.Name = "textBox_Interval3";
            this.textBox_Interval3.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval3.TabIndex = 38;
            this.textBox_Interval3.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(381, 153);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 39;
            this.label14.Text = "次数";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(381, 177);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "次数";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(446, 153);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 41;
            this.label16.Text = "间隔";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(446, 177);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 42;
            this.label17.Text = "间隔";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(511, 132);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 12);
            this.label18.TabIndex = 43;
            this.label18.Text = "s";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(511, 157);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 12);
            this.label19.TabIndex = 44;
            this.label19.Text = "s";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(511, 181);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 12);
            this.label20.TabIndex = 45;
            this.label20.Text = "s";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(511, 252);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 12);
            this.label21.TabIndex = 81;
            this.label21.Text = "s";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(511, 228);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 12);
            this.label22.TabIndex = 80;
            this.label22.Text = "s";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(511, 203);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 12);
            this.label23.TabIndex = 79;
            this.label23.Text = "s";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(446, 248);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 12);
            this.label24.TabIndex = 78;
            this.label24.Text = "间隔";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(446, 224);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 12);
            this.label25.TabIndex = 77;
            this.label25.Text = "间隔";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(381, 248);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 12);
            this.label26.TabIndex = 76;
            this.label26.Text = "次数";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(381, 224);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 75;
            this.label27.Text = "次数";
            // 
            // textBox_Interval6
            // 
            this.textBox_Interval6.Location = new System.Drawing.Point(475, 244);
            this.textBox_Interval6.Name = "textBox_Interval6";
            this.textBox_Interval6.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval6.TabIndex = 74;
            this.textBox_Interval6.Text = "0";
            // 
            // textBox_Times6
            // 
            this.textBox_Times6.Location = new System.Drawing.Point(412, 244);
            this.textBox_Times6.Name = "textBox_Times6";
            this.textBox_Times6.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times6.TabIndex = 73;
            this.textBox_Times6.Text = "0";
            // 
            // textBox_Interval5
            // 
            this.textBox_Interval5.Location = new System.Drawing.Point(475, 221);
            this.textBox_Interval5.Name = "textBox_Interval5";
            this.textBox_Interval5.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval5.TabIndex = 72;
            this.textBox_Interval5.Text = "0";
            // 
            // textBox_Times5
            // 
            this.textBox_Times5.Location = new System.Drawing.Point(412, 221);
            this.textBox_Times5.Name = "textBox_Times5";
            this.textBox_Times5.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times5.TabIndex = 71;
            this.textBox_Times5.Text = "0";
            // 
            // textBox_Interval4
            // 
            this.textBox_Interval4.Location = new System.Drawing.Point(475, 197);
            this.textBox_Interval4.Name = "textBox_Interval4";
            this.textBox_Interval4.Size = new System.Drawing.Size(34, 21);
            this.textBox_Interval4.TabIndex = 70;
            this.textBox_Interval4.Text = "0";
            // 
            // textBox_Times4
            // 
            this.textBox_Times4.Location = new System.Drawing.Point(412, 197);
            this.textBox_Times4.Name = "textBox_Times4";
            this.textBox_Times4.Size = new System.Drawing.Size(34, 21);
            this.textBox_Times4.TabIndex = 69;
            this.textBox_Times4.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(446, 200);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 68;
            this.label28.Text = "间隔";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(381, 200);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 67;
            this.label29.Text = "次数";
            // 
            // dateTimePicker_6
            // 
            this.dateTimePicker_6.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_6.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_6.Location = new System.Drawing.Point(276, 242);
            this.dateTimePicker_6.Name = "dateTimePicker_6";
            this.dateTimePicker_6.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_6.TabIndex = 66;
            this.dateTimePicker_6.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // dateTimePicker_5
            // 
            this.dateTimePicker_5.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_5.Location = new System.Drawing.Point(276, 219);
            this.dateTimePicker_5.Name = "dateTimePicker_5";
            this.dateTimePicker_5.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_5.TabIndex = 65;
            this.dateTimePicker_5.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // dateTimePicker_4
            // 
            this.dateTimePicker_4.CustomFormat = "HH:mm:ss";
            this.dateTimePicker_4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_4.Location = new System.Drawing.Point(276, 195);
            this.dateTimePicker_4.Name = "dateTimePicker_4";
            this.dateTimePicker_4.Size = new System.Drawing.Size(101, 21);
            this.dateTimePicker_4.TabIndex = 64;
            this.dateTimePicker_4.Value = new System.DateTime(2021, 3, 3, 0, 0, 0, 0);
            // 
            // textBox_y6
            // 
            this.textBox_y6.Location = new System.Drawing.Point(181, 242);
            this.textBox_y6.Name = "textBox_y6";
            this.textBox_y6.Size = new System.Drawing.Size(46, 21);
            this.textBox_y6.TabIndex = 63;
            this.textBox_y6.Text = "0";
            // 
            // textBox_y5
            // 
            this.textBox_y5.Location = new System.Drawing.Point(181, 218);
            this.textBox_y5.Name = "textBox_y5";
            this.textBox_y5.Size = new System.Drawing.Size(46, 21);
            this.textBox_y5.TabIndex = 62;
            this.textBox_y5.Text = "0";
            // 
            // textBox_y4
            // 
            this.textBox_y4.Location = new System.Drawing.Point(181, 194);
            this.textBox_y4.Name = "textBox_y4";
            this.textBox_y4.Size = new System.Drawing.Size(46, 21);
            this.textBox_y4.TabIndex = 61;
            this.textBox_y4.Text = "0";
            // 
            // textBox_x6
            // 
            this.textBox_x6.Location = new System.Drawing.Point(112, 242);
            this.textBox_x6.Name = "textBox_x6";
            this.textBox_x6.Size = new System.Drawing.Size(46, 21);
            this.textBox_x6.TabIndex = 60;
            this.textBox_x6.Text = "0";
            // 
            // textBox_x5
            // 
            this.textBox_x5.Location = new System.Drawing.Point(112, 218);
            this.textBox_x5.Name = "textBox_x5";
            this.textBox_x5.Size = new System.Drawing.Size(46, 21);
            this.textBox_x5.TabIndex = 59;
            this.textBox_x5.Text = "0";
            // 
            // textBox_x4
            // 
            this.textBox_x4.Location = new System.Drawing.Point(112, 194);
            this.textBox_x4.Name = "textBox_x4";
            this.textBox_x4.Size = new System.Drawing.Size(46, 21);
            this.textBox_x4.TabIndex = 58;
            this.textBox_x4.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(235, 247);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 12);
            this.label30.TabIndex = 57;
            this.label30.Text = "定时:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(235, 223);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 12);
            this.label31.TabIndex = 56;
            this.label31.Text = "定时:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(235, 199);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 12);
            this.label32.TabIndex = 55;
            this.label32.Text = "定时:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(163, 246);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 12);
            this.label33.TabIndex = 54;
            this.label33.Text = "y:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(163, 221);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 12);
            this.label34.TabIndex = 53;
            this.label34.Text = "y:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(163, 196);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 12);
            this.label35.TabIndex = 52;
            this.label35.Text = "y:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(64, 246);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 12);
            this.label36.TabIndex = 51;
            this.label36.Text = "坐标 x:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(64, 221);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 12);
            this.label37.TabIndex = 50;
            this.label37.Text = "坐标 x: ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(64, 196);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 12);
            this.label38.TabIndex = 49;
            this.label38.Text = "坐标 x:";
            // 
            // checkBox_6
            // 
            this.checkBox_6.AutoSize = true;
            this.checkBox_6.Location = new System.Drawing.Point(9, 245);
            this.checkBox_6.Name = "checkBox_6";
            this.checkBox_6.Size = new System.Drawing.Size(54, 16);
            this.checkBox_6.TabIndex = 48;
            this.checkBox_6.Text = "定时6";
            this.checkBox_6.UseVisualStyleBackColor = true;
            // 
            // checkBox_5
            // 
            this.checkBox_5.AutoSize = true;
            this.checkBox_5.Location = new System.Drawing.Point(9, 220);
            this.checkBox_5.Name = "checkBox_5";
            this.checkBox_5.Size = new System.Drawing.Size(54, 16);
            this.checkBox_5.TabIndex = 47;
            this.checkBox_5.Text = "定时5";
            this.checkBox_5.UseVisualStyleBackColor = true;
            // 
            // checkBox_4
            // 
            this.checkBox_4.AutoSize = true;
            this.checkBox_4.Location = new System.Drawing.Point(9, 196);
            this.checkBox_4.Name = "checkBox_4";
            this.checkBox_4.Size = new System.Drawing.Size(54, 16);
            this.checkBox_4.TabIndex = 46;
            this.checkBox_4.Text = "定时4";
            this.checkBox_4.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(253, 270);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 12);
            this.label39.TabIndex = 82;
            this.label39.Text = "句柄:";
            // 
            // textBox_Handle
            // 
            this.textBox_Handle.Location = new System.Drawing.Point(383, 266);
            this.textBox_Handle.Name = "textBox_Handle";
            this.textBox_Handle.Size = new System.Drawing.Size(92, 21);
            this.textBox_Handle.TabIndex = 83;
            // 
            // button_GETHandle
            // 
            this.button_GETHandle.Location = new System.Drawing.Point(488, 265);
            this.button_GETHandle.Name = "button_GETHandle";
            this.button_GETHandle.Size = new System.Drawing.Size(21, 23);
            this.button_GETHandle.TabIndex = 84;
            this.button_GETHandle.Text = "S";
            this.button_GETHandle.UseVisualStyleBackColor = true;
            this.button_GETHandle.Click += new System.EventHandler(this.button_GETHandle_Click);
            // 
            // checkBox_Handle
            // 
            this.checkBox_Handle.AutoSize = true;
            this.checkBox_Handle.Location = new System.Drawing.Point(235, 294);
            this.checkBox_Handle.Name = "checkBox_Handle";
            this.checkBox_Handle.Size = new System.Drawing.Size(72, 16);
            this.checkBox_Handle.TabIndex = 85;
            this.checkBox_Handle.Text = "句柄方式";
            this.checkBox_Handle.UseVisualStyleBackColor = true;
            this.checkBox_Handle.Click += new System.EventHandler(this.checkBox_Handle_Click);
            // 
            // textBox_HandleName
            // 
            this.textBox_HandleName.Location = new System.Drawing.Point(290, 266);
            this.textBox_HandleName.Name = "textBox_HandleName";
            this.textBox_HandleName.ReadOnly = true;
            this.textBox_HandleName.Size = new System.Drawing.Size(87, 21);
            this.textBox_HandleName.TabIndex = 86;
            // 
            // textBox_Handle1
            // 
            this.textBox_Handle1.Location = new System.Drawing.Point(112, 123);
            this.textBox_Handle1.Name = "textBox_Handle1";
            this.textBox_Handle1.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle1.TabIndex = 87;
            // 
            // textBox_Handle2
            // 
            this.textBox_Handle2.Location = new System.Drawing.Point(112, 147);
            this.textBox_Handle2.Name = "textBox_Handle2";
            this.textBox_Handle2.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle2.TabIndex = 88;
            // 
            // textBox_Handle3
            // 
            this.textBox_Handle3.Location = new System.Drawing.Point(112, 171);
            this.textBox_Handle3.Name = "textBox_Handle3";
            this.textBox_Handle3.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle3.TabIndex = 89;
            // 
            // textBox_Handle4
            // 
            this.textBox_Handle4.Location = new System.Drawing.Point(112, 194);
            this.textBox_Handle4.Name = "textBox_Handle4";
            this.textBox_Handle4.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle4.TabIndex = 90;
            // 
            // textBox_Handle5
            // 
            this.textBox_Handle5.Location = new System.Drawing.Point(112, 218);
            this.textBox_Handle5.Name = "textBox_Handle5";
            this.textBox_Handle5.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle5.TabIndex = 91;
            // 
            // textBox_Handle6
            // 
            this.textBox_Handle6.Location = new System.Drawing.Point(112, 242);
            this.textBox_Handle6.Name = "textBox_Handle6";
            this.textBox_Handle6.Size = new System.Drawing.Size(115, 21);
            this.textBox_Handle6.TabIndex = 92;
            // 
            // CheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 313);
            this.Controls.Add(this.textBox_Handle6);
            this.Controls.Add(this.textBox_Handle5);
            this.Controls.Add(this.textBox_Handle4);
            this.Controls.Add(this.textBox_Handle3);
            this.Controls.Add(this.textBox_Handle2);
            this.Controls.Add(this.textBox_Handle1);
            this.Controls.Add(this.textBox_HandleName);
            this.Controls.Add(this.checkBox_Handle);
            this.Controls.Add(this.button_GETHandle);
            this.Controls.Add(this.textBox_Handle);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.textBox_Interval6);
            this.Controls.Add(this.textBox_Times6);
            this.Controls.Add(this.textBox_Interval5);
            this.Controls.Add(this.textBox_Times5);
            this.Controls.Add(this.textBox_Interval4);
            this.Controls.Add(this.textBox_Times4);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.dateTimePicker_6);
            this.Controls.Add(this.dateTimePicker_5);
            this.Controls.Add(this.dateTimePicker_4);
            this.Controls.Add(this.textBox_y6);
            this.Controls.Add(this.textBox_y5);
            this.Controls.Add(this.textBox_y4);
            this.Controls.Add(this.textBox_x6);
            this.Controls.Add(this.textBox_x5);
            this.Controls.Add(this.textBox_x4);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.checkBox_6);
            this.Controls.Add(this.checkBox_5);
            this.Controls.Add(this.checkBox_4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox_Interval3);
            this.Controls.Add(this.textBox_Times3);
            this.Controls.Add(this.textBox_Interval2);
            this.Controls.Add(this.textBox_Times2);
            this.Controls.Add(this.textBox_Interval1);
            this.Controls.Add(this.textBox_Times1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CheckBox_Type);
            this.Controls.Add(this.label_Time);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateTimePicker_3);
            this.Controls.Add(this.dateTimePicker_2);
            this.Controls.Add(this.dateTimePicker_1);
            this.Controls.Add(this.button_pick);
            this.Controls.Add(this.textBox_y0);
            this.Controls.Add(this.textBox_x0);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox_y3);
            this.Controls.Add(this.textBox_y2);
            this.Controls.Add(this.textBox_y1);
            this.Controls.Add(this.textBox_x3);
            this.Controls.Add(this.textBox_x2);
            this.Controls.Add(this.textBox_x1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_3);
            this.Controls.Add(this.checkBox_2);
            this.Controls.Add(this.checkBox_1);
            this.Controls.Add(this.button_Start);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "CheckForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "定时点击";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CheckForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.CheckBox checkBox_1;
        private System.Windows.Forms.CheckBox checkBox_2;
        private System.Windows.Forms.CheckBox checkBox_3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_x1;
        private System.Windows.Forms.TextBox textBox_x2;
        private System.Windows.Forms.TextBox textBox_x3;
        private System.Windows.Forms.TextBox textBox_y1;
        private System.Windows.Forms.TextBox textBox_y2;
        private System.Windows.Forms.TextBox textBox_y3;
        private System.Windows.Forms.TextBox textBox_y0;
        private System.Windows.Forms.TextBox textBox_x0;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button_pick;
        private System.Windows.Forms.DateTimePicker dateTimePicker_1;
        private System.Windows.Forms.DateTimePicker dateTimePicker_2;
        private System.Windows.Forms.DateTimePicker dateTimePicker_3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.CheckBox CheckBox_Type;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_Times1;
        private System.Windows.Forms.TextBox textBox_Interval1;
        private System.Windows.Forms.TextBox textBox_Times2;
        private System.Windows.Forms.TextBox textBox_Interval2;
        private System.Windows.Forms.TextBox textBox_Times3;
        private System.Windows.Forms.TextBox textBox_Interval3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox_Interval6;
        private System.Windows.Forms.TextBox textBox_Times6;
        private System.Windows.Forms.TextBox textBox_Interval5;
        private System.Windows.Forms.TextBox textBox_Times5;
        private System.Windows.Forms.TextBox textBox_Interval4;
        private System.Windows.Forms.TextBox textBox_Times4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dateTimePicker_6;
        private System.Windows.Forms.DateTimePicker dateTimePicker_5;
        private System.Windows.Forms.DateTimePicker dateTimePicker_4;
        private System.Windows.Forms.TextBox textBox_y6;
        private System.Windows.Forms.TextBox textBox_y5;
        private System.Windows.Forms.TextBox textBox_y4;
        private System.Windows.Forms.TextBox textBox_x6;
        private System.Windows.Forms.TextBox textBox_x5;
        private System.Windows.Forms.TextBox textBox_x4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox checkBox_6;
        private System.Windows.Forms.CheckBox checkBox_5;
        private System.Windows.Forms.CheckBox checkBox_4;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox_Handle;
        private System.Windows.Forms.Button button_GETHandle;
        private System.Windows.Forms.CheckBox checkBox_Handle;
        private System.Windows.Forms.TextBox textBox_HandleName;
        private System.Windows.Forms.TextBox textBox_Handle1;
        private System.Windows.Forms.TextBox textBox_Handle2;
        private System.Windows.Forms.TextBox textBox_Handle3;
        private System.Windows.Forms.TextBox textBox_Handle4;
        private System.Windows.Forms.TextBox textBox_Handle5;
        private System.Windows.Forms.TextBox textBox_Handle6;
    }
}

