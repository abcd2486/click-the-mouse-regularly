using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static MouseClicker.Win32Api;

namespace MouseClicker
{
    public partial class CheckForm : Form
    {
        /// <summary>
        /// 配置文件访问
        /// </summary>
        public AppConfigManager apm = new AppConfigManager();

        private MouseHook m_mh1;
        private MouseHook m_mh2;
        private List<WindowInfo> wndList = new List<WindowInfo>();

        /// <summary>
        /// 定义一个队列，用于记录用户创建的线程
        /// 以便在窗体关闭的时候关闭所有用于创建的线程
        /// </summary>
        private List<Thread> m_ChaosThreadList;

        private bool bStart = false;

        /// <summary>
        /// 定义一个代理
        /// </summary>
        /// <param name="MSG"></param>
        private delegate void DispMSGDelegate(string MSG);

        public CheckForm()
        {
            InitializeComponent();
            this.Hide();
            LoadForm();
            timer1.Enabled = true;
            m_ChaosThreadList = new List<Thread>();
        }

        private void LoadForm()
        {
            //1
            string strcheckBox1 = apm.GetAppSetting("checkBox1");
            if (strcheckBox1 == "true" || strcheckBox1 == "True")
            {
                checkBox_1.Checked = true;
            }
            else
            {
                checkBox_1.Checked = false;
            }

            int nTemp = 0;
            textBox_x1.Text = int.TryParse(apm.GetAppSetting("textBox_x1"), out nTemp) ? apm.GetAppSetting("textBox_x1") : "0";
            textBox_y1.Text = int.TryParse(apm.GetAppSetting("textBox_y1"), out nTemp) ? apm.GetAppSetting("textBox_y1") : "0";

            //2
            string strcheckBox2 = apm.GetAppSetting("checkBox2");
            if (strcheckBox2 == "true" || strcheckBox2 == "True")
            {
                checkBox_2.Checked = true;
            }
            else
            {
                checkBox_2.Checked = false;
            }
            textBox_x2.Text = int.TryParse(apm.GetAppSetting("textBox_x2"), out nTemp) ? apm.GetAppSetting("textBox_x2") : "0";
            textBox_y2.Text = int.TryParse(apm.GetAppSetting("textBox_y2"), out nTemp) ? apm.GetAppSetting("textBox_y2") : "0";

            //3
            string strcheckBox3 = apm.GetAppSetting("checkBox3");
            if (strcheckBox3 == "true" || strcheckBox3 == "True")
            {
                checkBox_3.Checked = true;
            }
            else
            {
                checkBox_3.Checked = false;
            }
            textBox_x3.Text = int.TryParse(apm.GetAppSetting("textBox_x3"), out nTemp) ? apm.GetAppSetting("textBox_x3") : "0";
            textBox_y3.Text = int.TryParse(apm.GetAppSetting("textBox_y3"), out nTemp) ? apm.GetAppSetting("textBox_y3") : "0";

            //4
            string strcheckBox4 = apm.GetAppSetting("checkBox4");
            if (strcheckBox4 == "true" || strcheckBox4 == "True")
            {
                checkBox_4.Checked = true;
            }
            else
            {
                checkBox_4.Checked = false;
            }
            textBox_x4.Text = int.TryParse(apm.GetAppSetting("textBox_x4"), out nTemp) ? apm.GetAppSetting("textBox_x4") : "0";
            textBox_y4.Text = int.TryParse(apm.GetAppSetting("textBox_y4"), out nTemp) ? apm.GetAppSetting("textBox_y4") : "0";

            //5
            string strcheckBox5 = apm.GetAppSetting("checkBox5");
            if (strcheckBox5 == "true" || strcheckBox5 == "True")
            {
                checkBox_5.Checked = true;
            }
            else
            {
                checkBox_5.Checked = false;
            }
            textBox_x5.Text = int.TryParse(apm.GetAppSetting("textBox_x5"), out nTemp) ? apm.GetAppSetting("textBox_x5") : "0";
            textBox_y5.Text = int.TryParse(apm.GetAppSetting("textBox_y5"), out nTemp) ? apm.GetAppSetting("textBox_y5") : "0";

            //6
            string strcheckBox6 = apm.GetAppSetting("checkBox6");
            if (strcheckBox6 == "true" || strcheckBox6 == "True")
            {
                checkBox_6.Checked = true;
            }
            else
            {
                checkBox_6.Checked = false;
            }
            textBox_x6.Text = int.TryParse(apm.GetAppSetting("textBox_x6"), out nTemp) ? apm.GetAppSetting("textBox_x6") : "0";
            textBox_y6.Text = int.TryParse(apm.GetAppSetting("textBox_y6"), out nTemp) ? apm.GetAppSetting("textBox_y6") : "0";

            string checkBox_Type = apm.GetAppSetting("CheckBox_Type");
            if (checkBox_Type == "true" || checkBox_Type == "True")
            {
                CheckBox_Type.Checked = true;
            }
            else
            {
                CheckBox_Type.Checked = false;
            }

            string strcheckBox_Handle = apm.GetAppSetting("checkBox_Handle");
            if (strcheckBox_Handle == "true" || strcheckBox_Handle == "True")
            {
                checkBox_Handle.Checked = true;
            }
            else
            {
                checkBox_Handle.Checked = false;
            }

            textBox_Times1.Text = int.TryParse(apm.GetAppSetting("textBox_Times1"), out nTemp) ? apm.GetAppSetting("textBox_Times1") : "1";
            textBox_Times2.Text = int.TryParse(apm.GetAppSetting("textBox_Times2"), out nTemp) ? apm.GetAppSetting("textBox_Times2") : "1";
            textBox_Times3.Text = int.TryParse(apm.GetAppSetting("textBox_Times3"), out nTemp) ? apm.GetAppSetting("textBox_Times3") : "1";
            textBox_Times4.Text = int.TryParse(apm.GetAppSetting("textBox_Times4"), out nTemp) ? apm.GetAppSetting("textBox_Times4") : "1";
            textBox_Times5.Text = int.TryParse(apm.GetAppSetting("textBox_Times5"), out nTemp) ? apm.GetAppSetting("textBox_Times5") : "1";
            textBox_Times6.Text = int.TryParse(apm.GetAppSetting("textBox_Times6"), out nTemp) ? apm.GetAppSetting("textBox_Times6") : "1";
            textBox_Interval1.Text = int.TryParse(apm.GetAppSetting("textBox_Interval1"), out nTemp) ? apm.GetAppSetting("textBox_Interval1") : "1";
            textBox_Interval2.Text = int.TryParse(apm.GetAppSetting("textBox_Interval2"), out nTemp) ? apm.GetAppSetting("textBox_Interval2") : "1";
            textBox_Interval3.Text = int.TryParse(apm.GetAppSetting("textBox_Interval3"), out nTemp) ? apm.GetAppSetting("textBox_Interval3") : "1";
            textBox_Interval4.Text = int.TryParse(apm.GetAppSetting("textBox_Interval4"), out nTemp) ? apm.GetAppSetting("textBox_Interval4") : "1";
            textBox_Interval5.Text = int.TryParse(apm.GetAppSetting("textBox_Interval5"), out nTemp) ? apm.GetAppSetting("textBox_Interval5") : "1";
            textBox_Interval6.Text = int.TryParse(apm.GetAppSetting("textBox_Interval6"), out nTemp) ? apm.GetAppSetting("textBox_Interval6") : "1";

            checkBox_HandleInte();
        }



        /// <summary>
        /// 移动鼠标到指定的坐标点
        /// </summary>
        public void MoveMouseToPoint(Point p)
        {
            Win32Api.SetCursorPos(p.X, p.Y);
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            string buttonText = button_Start.Text;

            SaveInI();

            if (buttonText == "开始")
            {
                bStart = true;
                string strTemp = "";
                if (checkBox_1.Checked == true)
                {
                    strTemp += "定时器1 ";
                }
                if (checkBox_2.Checked == true)
                {
                    strTemp += "定时器2 ";
                }
                if (checkBox_3.Checked == true)
                {
                    strTemp += "定时器3 ";
                }
                if (checkBox_4.Checked == true)
                {
                    strTemp += "定时器4 ";
                }
                if (checkBox_5.Checked == true)
                {
                    strTemp += "定时器5 ";
                }
                if (checkBox_6.Checked == true)
                {
                    strTemp += "定时器6 ";
                }
                WriteLog("开始运行：" + strTemp);
                button_Start.Text = "停止";

                checkBox_1.Enabled = false;
                checkBox_2.Enabled = false;
                checkBox_3.Enabled = false;
                checkBox_4.Enabled = false;
                checkBox_5.Enabled = false;
                checkBox_6.Enabled = false;
            }
            else
            {
                bStart = false;
                button_Start.Text = "开始";
                WriteLog("全部停止");
                ClosedThread();

                checkBox_1.Enabled = true;
                checkBox_2.Enabled = true;
                checkBox_3.Enabled = true;
                checkBox_4.Enabled = true;
                checkBox_5.Enabled = true;
                checkBox_6.Enabled = true;
            }
        }

        private void SaveInI()
        {
            //1
            apm.SaveAppSetting("checkBox1", checkBox_1.Checked.ToString());
            apm.SaveAppSetting("textBox_x1", textBox_x1.Text);
            apm.SaveAppSetting("textBox_y1", textBox_y1.Text);
            apm.SaveAppSetting("dateTimePicker1", dateTimePicker_1.Text);
            //2
            apm.SaveAppSetting("checkBox2", checkBox_2.Checked.ToString());
            apm.SaveAppSetting("textBox_x2", textBox_x2.Text);
            apm.SaveAppSetting("textBox_y2", textBox_y2.Text);
            apm.SaveAppSetting("dateTimePicker2", dateTimePicker_2.Text);
            //3
            apm.SaveAppSetting("checkBox3", checkBox_3.Checked.ToString());
            apm.SaveAppSetting("textBox_x3", textBox_x3.Text);
            apm.SaveAppSetting("textBox_y3", textBox_y3.Text);
            apm.SaveAppSetting("dateTimePicker3", dateTimePicker_3.Text);
            //4
            apm.SaveAppSetting("checkBox4", checkBox_4.Checked.ToString());
            apm.SaveAppSetting("textBox_x4", textBox_x4.Text);
            apm.SaveAppSetting("textBox_y4", textBox_y4.Text);
            apm.SaveAppSetting("dateTimePicker4", dateTimePicker_4.Text);
            //5
            apm.SaveAppSetting("checkBox5", checkBox_5.Checked.ToString());
            apm.SaveAppSetting("textBox_x5", textBox_x5.Text);
            apm.SaveAppSetting("textBox_y5", textBox_y5.Text);
            apm.SaveAppSetting("dateTimePicker5", dateTimePicker_5.Text);
            //6
            apm.SaveAppSetting("checkBox6", checkBox_6.Checked.ToString());
            apm.SaveAppSetting("textBox_x6", textBox_x6.Text);
            apm.SaveAppSetting("textBox_y6", textBox_y6.Text);
            apm.SaveAppSetting("dateTimePicker6", dateTimePicker_6.Text);

            apm.SaveAppSetting("CheckBox_Type", CheckBox_Type.Checked.ToString());

            apm.SaveAppSetting("checkBox_Handle", checkBox_Handle.Checked.ToString());

            apm.SaveAppSetting("textBox_Times1", textBox_Times1.Text);
            apm.SaveAppSetting("textBox_Times2", textBox_Times2.Text);
            apm.SaveAppSetting("textBox_Times3", textBox_Times3.Text);
            apm.SaveAppSetting("textBox_Times4", textBox_Times4.Text);
            apm.SaveAppSetting("textBox_Times5", textBox_Times5.Text);
            apm.SaveAppSetting("textBox_Times6", textBox_Times6.Text);
            apm.SaveAppSetting("textBox_Interval1", textBox_Interval1.Text);
            apm.SaveAppSetting("textBox_Interval2", textBox_Interval2.Text);
            apm.SaveAppSetting("textBox_Interval3", textBox_Interval3.Text);
            apm.SaveAppSetting("textBox_Interval4", textBox_Interval4.Text);
            apm.SaveAppSetting("textBox_Interval5", textBox_Interval5.Text);
            apm.SaveAppSetting("textBox_Interval6", textBox_Interval6.Text);
        }

        private void DoCheck(object param)
        {
            // 将object转成数组
            object[] objArr = (object[])param;
            string strType = (string)objArr[0];
            string strX = (string)objArr[1]; ;
            string strY = (string)objArr[2]; ;
            int nTexTimes = (int)objArr[3];
            int nInterval = (int)objArr[4];

            int nTimes = 0;

            while (nTimes < nTexTimes)
            {
                int loginx;
                int.TryParse(strX, out loginx);

                int loginy;
                int.TryParse(strY, out loginy);
                Point centerP = new Point(loginx, loginy);
                //移动鼠标
                MoveMouseToPoint(centerP);
                Thread.Sleep(0);
                Win32Api.mouse_event(Win32Api.MOUSEEVENTF_LEFTDOWN | Win32Api.MOUSEEVENTF_LEFTUP, loginx, loginy, 0, 0);
                WriteLog(String.Format(strType + "鼠标方式 x:{0} y:{1} 第{2}次", loginx, loginy, nTimes + 1));
                if (nTexTimes - nTimes > 1)
                {
                    Thread.Sleep(nInterval * 1000);
                }
                nTimes++;
            }
        }

        private void mh_MouseClickEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                textBox_x0.Text = e.X.ToString();
                textBox_y0.Text = e.Y.ToString();
            }
        }

        private void mh_MouseDoubleClickEvent(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            //{
            //当前鼠标位置
            int x = e.Location.X;
            int y = e.Location.Y;

            IntPtr hwnd = Win32Api.WindowFromPoint(x, y);//获取指定坐标处窗口的句柄

            StringBuilder sb = new StringBuilder(256);
            GetWindowText(hwnd, sb, sb.Capacity);

            textBox_Handle.Text = hwnd.ToInt64().ToString();
            textBox_HandleName.Text = sb.ToString();

            //res(hwnd);
            //}
        }

        public Boolean EnumProc(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder sb = new StringBuilder(256);
            WindowInfo wnd = new WindowInfo();
            wnd.hWnd = hWnd;                    //get window name
            GetWindowText(hWnd, sb, sb.Capacity);
            wnd.szWindowName = sb.ToString();                    //get window class
            GetClassName(hWnd, sb, sb.Capacity);
            wnd.szClassName = sb.ToString();                    //add it into list
            wndList.Add(wnd);
            return true;
        }

        /// <summary>
        /// 遍历获取指定窗体上的所有句柄
        /// </summary>
        /// <param name="hwnd"></param>
        /// <returns></returns>
        public List<WindowInfo> res(IntPtr hwnd)
        {
            wndList.Clear();
            EnumWindowsCallBack CallBackProc = new EnumWindowsCallBack(EnumProc);
            EnumChildWindows(hwnd, CallBackProc, 0);
            return wndList;
        }

        public class WindowInfo
        {
            public IntPtr hWnd { get; set; }
            public String szWindowName { get; set; }
            public String szClassName { get; set; }
            public String valuename { get; set; }
        }

        private void mh_MouseMoveEvent(object sender, MouseEventArgs e)
        {
            int x = e.Location.X;
            int y = e.Location.Y;
            textBox_x0.Text = x + "";
            textBox_y0.Text = y + "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime nowTime = DateTime.Now;
            label_Time.Text = "当前时间:" + nowTime.ToString("yyyy:MM:dd HH:mm:ss");

            if (bStart)
            {
                try
                {
                    if (checkBox_1.Checked == true)
                    {
                        int nHour = dateTimePicker_1.Value.Hour;
                        int nMinute = dateTimePicker_1.Value.Minute;
                        int nSecond = dateTimePicker_1.Value.Second;

                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times1.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval1.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle1.Text, out nHandle);

                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时一：";
                            objectArray[1] = textBox_x1.Text;
                            objectArray[2] = textBox_y1.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                    if (checkBox_2.Checked == true)
                    {
                        int nHour = dateTimePicker_2.Value.Hour;
                        int nMinute = dateTimePicker_2.Value.Minute;
                        int nSecond = dateTimePicker_2.Value.Second;
                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times2.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval2.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle2.Text, out nHandle);
                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时二：";
                            objectArray[1] = textBox_x2.Text;
                            objectArray[2] = textBox_y2.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                    if (checkBox_3.Checked == true)
                    {
                        int nHour = dateTimePicker_3.Value.Hour;
                        int nMinute = dateTimePicker_3.Value.Minute;
                        int nSecond = dateTimePicker_3.Value.Second;
                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times3.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval3.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle3.Text, out nHandle);
                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时三：";
                            objectArray[1] = textBox_x3.Text;
                            objectArray[2] = textBox_y3.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                    if (checkBox_4.Checked == true)
                    {
                        int nHour = dateTimePicker_4.Value.Hour;
                        int nMinute = dateTimePicker_4.Value.Minute;
                        int nSecond = dateTimePicker_4.Value.Second;
                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times4.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval4.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle4.Text, out nHandle);
                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时四：";
                            objectArray[1] = textBox_x4.Text;
                            objectArray[2] = textBox_y4.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                    if (checkBox_5.Checked == true)
                    {
                        int nHour = dateTimePicker_5.Value.Hour;
                        int nMinute = dateTimePicker_5.Value.Minute;
                        int nSecond = dateTimePicker_5.Value.Second;
                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times5.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval5.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle5.Text, out nHandle);
                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时五：";
                            objectArray[1] = textBox_x5.Text;
                            objectArray[2] = textBox_y5.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                    if (checkBox_6.Checked == true)
                    {
                        int nHour = dateTimePicker_6.Value.Hour;
                        int nMinute = dateTimePicker_6.Value.Minute;
                        int nSecond = dateTimePicker_6.Value.Second;
                        if (nowTime.Hour == nHour
                            && nowTime.Minute == nMinute
                            && (nowTime.Second >= nSecond && nowTime.Second < nSecond + 1))
                        {
                            int nTexTimes;
                            int.TryParse(textBox_Times6.Text, out nTexTimes);
                            int nInterval;
                            int.TryParse(textBox_Interval6.Text, out nInterval);
                            int nHandle;
                            int.TryParse(textBox_Handle6.Text, out nHandle);
                            object[] objectArray = new object[6];//这里的5就是改成你要传递几个参数
                            objectArray[0] = "定时六：";
                            objectArray[1] = textBox_x6.Text;
                            objectArray[2] = textBox_y6.Text;
                            objectArray[3] = nTexTimes;
                            objectArray[4] = nInterval;
                            objectArray[5] = nHandle;
                            object param = objectArray;

                            if (checkBox_Handle.Checked)
                            {
                                Thread thread = new Thread(HandleCheck);
                                m_ChaosThreadList.Add(thread);
                                thread.Start(param);
                            }
                            else
                            {
                                if (CheckBox_Type.Checked)
                                {
                                    Thread thread = new Thread(PostCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                                else
                                {
                                    Thread thread = new Thread(DoCheck);
                                    m_ChaosThreadList.Add(thread);
                                    thread.Start(param);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteLog(String.Format("错误：{0}", ex.ToString()));
                }
            }
        }

        private void button_pick_Click(object sender, EventArgs e)
        {
            string buttonText = button_pick.Text;
            if (buttonText == "S")
            {
                button_pick.Text = "E";

                m_mh1 = new MouseHook();
                m_mh1.SetHook();
                m_mh1.MouseMoveEvent += mh_MouseMoveEvent;
                m_mh1.MouseClickEvent += mh_MouseClickEvent;
            }
            else
            {
                button_pick.Text = "S";
                m_mh1.UnHook();
            }
        }

        private void PostCheck(object param)
        {
            // 将object转成数组
            object[] objArr = (object[])param;
            string strType = (string)objArr[0];
            string text1 = (string)objArr[1]; ;
            string text2 = (string)objArr[2]; ;
            int nTexTimes = (int)objArr[3];
            int nInterval = (int)objArr[4];

            int nTimes = 0;

            while (nTimes < nTexTimes)
            {
                int nX;
                int.TryParse(text1, out nX);
                int nY;
                int.TryParse(text2, out nY);
                IntPtr m_hWnd = Win32Api.WindowFromPoint(nX, nY);

                StringBuilder sb = new StringBuilder(256);
                GetWindowText(m_hWnd, sb, sb.Capacity);

                int sd = Win32Api.PostMessage(m_hWnd, Win32Api.WM_CLICK, 0x1, 1 + (1 << 16));//传递2个整型参数成功
                //int sd = Win32Api.SendMessage(m_hWnd, Win32Api.WM_CLICK, 0x1, nX + (nY << 16));//传递2个整型参数成功

                WriteLog(strType + String.Format("消息方式 {0} x:{1} y:{2} 第{3}次", sb.ToString(), nX, nY, nTimes + 1));
                if (nTexTimes - nTimes > 1)
                {
                    Thread.Sleep(nInterval * 1000);
                }

                nTimes++;
            }
        }

        private void HandleCheck(object param)
        {
            // 将object转成数组
            object[] objArr = (object[])param;
            string strType = (string)objArr[0];
            string text1 = (string)objArr[1]; ;
            string text2 = (string)objArr[2]; ;
            int nTexTimes = (int)objArr[3];
            int nInterval = (int)objArr[4];
            int nHandle = (int)objArr[5];

            int nTimes = 0;

            while (nTimes < nTexTimes)
            {
                StringBuilder sb = new StringBuilder(256);
                try
                {
                    IntPtr m_hWnd = new IntPtr(nHandle);
                    //SetForegroundWindow(m_hWnd);
                    GetWindowText(m_hWnd, sb, sb.Capacity);

                    //int sd = Win32Api.PostMessage(m_hWnd, Win32Api.WM_CLICK, 0x1, 1 + (1 << 16));//传递2个整型参数成功
                    int sd = Win32Api.SendMessage(m_hWnd, Win32Api.WM_CLICK, 0x1, 1 + (1 << 16));//传递2个整型参数成功
                    WriteLog(strType + String.Format("句柄:{0} 第{1}次", sb.ToString(), nTimes + 1));
                    if (nTexTimes - nTimes > 1)
                    {
                        Thread.Sleep(nInterval * 1000);
                    }
                }
                catch (Exception e)
                {
                    WriteLog(strType + String.Format("句柄:{0} 第{1}次 错误{2}", sb.ToString(), nTimes + 1, e.ToString()));
                }
                nTimes++;
            }
        }

        private void WriteLog(string strlog)
        {
            if (this.textBox1.InvokeRequired == false)     //如果调用该函数的线程和控件lstMain位于同一个线程内
            {
                textBox1.AppendText("\r\n" + DateTime.Now.ToString("MM:dd HH:mm:ss  ") + strlog);     // 追加文本，并且使得光标定位到插入地方。
                textBox1.ScrollToCaret();
            }
            else                                        //如果调用该函数的线程和控件lstMain不在同一个线程
            {
                //通过使用Invoke的方法，让子线程告诉窗体线程来完成相应的控件操作
                DispMSGDelegate DMSGD = new DispMSGDelegate(WriteLog);

                //使用控件lstMain的Invoke方法执行DMSGD代理(其类型是DispMSGDelegate)
                this.textBox1.Invoke(DMSGD, strlog);
            }
        }

        private void CheckForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClosedThread();
        }

        private void ClosedThread()
        {
            if (m_ChaosThreadList.Count > 0)
            {
                try
                {
                    //编列自定义队列,将所有线程终止
                    foreach (Thread tWorkingThread in m_ChaosThreadList)
                    {
                        tWorkingThread.Abort();
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            m_ChaosThreadList.Clear();
        }

        private void button_GETHandle_Click(object sender, EventArgs e)
        {
            string buttonText = button_GETHandle.Text;
            if (buttonText == "S")
            {
                button_GETHandle.Text = "E";

                m_mh2 = new MouseHook();
                m_mh2.SetHook();
                m_mh2.MouseMoveEvent += mh_MouseDoubleClickEvent;
            }
            else
            {
                button_GETHandle.Text = "S";
                m_mh2.UnHook();
            }
        }

        private void checkBox_Handle_Click(object sender, EventArgs e)
        {
            checkBox_HandleInte();
        }

        private void checkBox_HandleInte()
        {
            if (checkBox_Handle.Checked == true)
            {
                CheckBox_Type.Enabled = false;
                textBox_Handle.Enabled = true;
                button_GETHandle.Enabled = true;
                label1.Text = "句柄:";
                label2.Text = "句柄:";
                label3.Text = "句柄:";
                label38.Text = "句柄:";
                label37.Text = "句柄:";
                label36.Text = "句柄:";

                label4.Visible = false;
                label5.Visible = false;
                label6.Visible = false;
                label35.Visible = false;
                label34.Visible = false;
                label33.Visible = false;

                textBox_x1.Visible = false;
                textBox_x2.Visible = false;
                textBox_x3.Visible = false;
                textBox_x4.Visible = false;
                textBox_x5.Visible = false;
                textBox_x6.Visible = false;
                textBox_y1.Visible = false;
                textBox_y2.Visible = false;
                textBox_y3.Visible = false;
                textBox_y4.Visible = false;
                textBox_y5.Visible = false;
                textBox_y6.Visible = false;

                textBox_Handle1.Visible = true;
                textBox_Handle2.Visible = true;
                textBox_Handle3.Visible = true;
                textBox_Handle4.Visible = true;
                textBox_Handle5.Visible = true;
                textBox_Handle6.Visible = true;
            }
            else
            {
                CheckBox_Type.Enabled = true;
                textBox_Handle.Enabled = false;
                button_GETHandle.Enabled = false;
                label1.Text = "坐标 x:";
                label2.Text = "坐标 x:";
                label3.Text = "坐标 x:";
                label38.Text = "坐标 x:";
                label37.Text = "坐标 x:";
                label36.Text = "坐标 x:";

                label4.Visible = true;
                label5.Visible = true;
                label6.Visible = true;
                label35.Visible = true;
                label34.Visible = true;
                label33.Visible = true;

                textBox_x1.Visible = true;
                textBox_x2.Visible = true;
                textBox_x3.Visible = true;
                textBox_x4.Visible = true;
                textBox_x5.Visible = true;
                textBox_x6.Visible = true;
                textBox_y1.Visible = true;
                textBox_y2.Visible = true;
                textBox_y3.Visible = true;
                textBox_y4.Visible = true;
                textBox_y5.Visible = true;
                textBox_y6.Visible = true;

                textBox_Handle1.Visible = false;
                textBox_Handle2.Visible = false;
                textBox_Handle3.Visible = false;
                textBox_Handle4.Visible = false;
                textBox_Handle5.Visible = false;
                textBox_Handle6.Visible = false;
            }
        }

        private void CheckForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    button_pick_Click(this, EventArgs.Empty);
                    break;

                case Keys.F2:
                    button_GETHandle_Click(this, EventArgs.Empty);
                    break;
            }
        }
    }
}