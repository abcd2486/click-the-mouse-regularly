using System;
using System.Windows.Forms;

namespace MouseClicker
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form mainForm = new CheckForm();
            Application.Run(mainForm);
        }
    }
}