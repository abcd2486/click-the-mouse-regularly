# 鼠标定时点击

#### 介绍
鼠标定时点击，鼠标方式为模拟移动鼠标进行点击；消息方式为直接发送点击消息，可用于云服务器等。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/184326_2119e215_1871411.png "1.png")

52破解网址：https://www.52pojie.cn/thread-1428709-1-1.html
#### 软件架构
c#


#### 安装教程

1.  管理员身份运行

#### 使用说明

1.  快捷键F1：拾取屏幕点坐标，再点击一次停止拾取。
2.  快捷键F2：拾取控件句柄，再点击一次停止拾取。
3.  消息模式：调用windowapi SendMessage直接发送点击消息。
4.  未勾选消息模式时，默认鼠标模式，通过api移动鼠标到输入的坐标点进行点击。
5.  勾选句柄模式，会以输入的句柄进行发送消息点击。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
